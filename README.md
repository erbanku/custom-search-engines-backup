# Custom Search Engines Backup & Sync

![777](https://user-images.githubusercontent.com/79493862/138395266-146e3658-3b0c-4528-b394-87a742518267.png)


Import/Export Google Chrome or Microsoft Edge's custom search engines and sync (overwrite) between 2 browsers.

- [Import Export Chrome Custom Search Engines](https://github.com/erbanku/custom-search-engines-backup/tree/main/Import%20Export%20Chrome%20Custom%20Search%20Engines)
- [Import Export Edge Custom Search Engines](https://github.com/erbanku/custom-search-engines-backup/tree/main/Import%20Export%20Edge%20Custom%20Search%20Engines)
- [Overwrite Chrome Custom Search Engines to Edge](https://github.com/erbanku/custom-search-engines-backup/tree/main/Overwrite%20Chrome%20Custom%20Search%20Engines%20to%20Edge)
- [Overwrite Edge Custom Search Engines to Chrome](https://github.com/erbanku/custom-search-engines-backup/tree/main/Overwrite%20Edge%20Custom%20Search%20Engines%20to%20Chrome)

## Thanks

Thanks to [ludovicchabant](https://github.com/ludovicchabant) for [ludovicchabant/Chrome-Search-Engine-Sync](https://github.com/ludovicchabant/Chrome-Search-Engine-Sync)

